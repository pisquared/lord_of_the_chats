#!/usr/bin/env bash

set -e
set +x

echo "Provisioning sensitive file"
echo "======================================="
cp sensitive.py.sample sensitive.py

echo "Installing virtualenv"
echo "======================================="
sudo apt-get update
sudo apt-get install -y python-virtualenv

echo "Creating virtualenv"
echo "======================================="
virtualenv -p python3 venv
source venv/bin/activate

echo "Installing python packages"
echo "======================================="
pip install -r requirements.txt

echo "Patching flask_markdown"
echo "======================================="
patch venv/lib/python3.5/site-packages/flask_cache/jinja2ext.py provision/flask_cache_jinja.patch
patch venv/lib/python3.5/site-packages/flask_sqlalchemy_cache/core.py provision/flask_sql_cache_core.patch

echo "Init db"
echo "======================================="
export FLASK_APP="webapp.py"

echo "Populating db"
echo "======================================="
source venv/bin/activate && python populate.py

echo "Download Signal-CLI"
echo "======================================="
SIGNAL_VERSION="0.6.0"
SIGNAL_BIN_DIR="provider_proxies/signal"
mkdir -p "${SIGNAL_BIN_DIR}"
wget https://github.com/AsamK/signal-cli/releases/download/v"${SIGNAL_VERSION}"/signal-cli-"${SIGNAL_VERSION}".tar.gz -P "${SIGNAL_BIN_DIR}"
tar xfvz "${SIGNAL_BIN_DIR}"/signal-cli-"${SIGNAL_VERSION}".tar.gz -C "${SIGNAL_BIN_DIR}"
rm "${SIGNAL_BIN_DIR}"/signal-cli-"${SIGNAL_VERSION}".tar.gz