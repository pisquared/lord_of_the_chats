import time

from flask_security import SQLAlchemyUserDatastore
from flask_security.utils import hash_password

import sensitive
from webapp.models import Role, User, Provider, Contact, Channel
from webapp.store import db
from webapp import app

db.init_app(app)

user_datastore = SQLAlchemyUserDatastore(db, User, Role)

app.app_context().push()
db.create_all()

# TODO: Handle DST
user = user_datastore.create_user(email=u'admin@app.com',
                                  password=hash_password(sensitive.ADMIN_PASSWORD),
                                  timezone=time.tzname[0],
                                  tz_offset_seconds=-time.timezone)
me_channel = Channel.create(name="Me",
                            user=user, )
me_contact = Contact.create(first_name="Daniel",
                            last_name="Tsvetkov",
                            user=user,
                            default_channel=me_channel,
                            me=True,
                            )

Provider.create(name="Lord of the Chats", proxy_name="lotc")  # 1
Provider.create(name="Telegram", proxy_name="telegram")  # 2
Provider.create(name="Signal", proxy_name="signal")  # 3
Provider.create(name="Viber", proxy_name="viber")  # 4
Provider.create(name="Google Hangouts", proxy_name="hangouts")  # 5
Provider.create(name="Slack", proxy_name="slack")  # 6
Provider.create(name="Facebook Messenger", proxy_name="messenger")  # 7
Provider.create(name="Whatsapp", proxy_name="whatsapp")  # 8
Provider.create(name="Skype", proxy_name="skype")  # 9
Provider.create(name="LinkedIn", proxy_name="linkedin")  # 10
Provider.create(name="Meetup", proxy_name="meetup")  # 11

db.session.commit()
