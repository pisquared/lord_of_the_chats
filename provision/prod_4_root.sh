#!/usr/bin/env bash
set -e

cd /var/www/mafkingprod.com

echo "REMOTE: Installing gunicorn service"
echo "======================================="
cp provision/mafkingprod.service /etc/systemd/system/
systemctl daemon-reload
systemctl enable mafkingprod
systemctl start mafkingprod

if [ ! -f /etc/letsencrypt/live/mafkingprod.com/fullchain.pem ]; then
    echo "REMOTE: Generating letsencrypt certificate"
    echo "======================================="
    [ ! -f /tmp/certbot-auto ] && wget -O /tmp/certbot-auto https://dl.eff.org/certbot-auto
    chmod +x /tmp/certbot-auto
    /tmp/certbot-auto --authenticator standalone --installer nginx --pre-hook "service nginx stop" --post-hook "service nginx start" --redirect --agree-tos --no-eff-email --email admin@app.com -d mafkingprod.com -d www.mafkingprod.com --no-bootstrap
fi

echo "REMOTE: Copying over nginx config"
echo "======================================="
rm /etc/nginx/sites-enabled/default
cp provision/mafkingprod_nginx.conf /etc/nginx/sites-available/mafkingprod.conf
[ ! -f /etc/nginx/sites-enabled/mafkingprod.conf ] && \
  ln -s /etc/nginx/sites-available/mafkingprod.conf /etc/nginx/sites-enabled/mafkingprod.conf && \
  systemctl restart nginx

echo "REMOTE: Enable IPv6"
echo "======================================="
cat > /etc/network/interfaces << EOF
iface eth0 inet6 static
        address 2a03:b0c0:3:d0::b3f:f001
        netmask 64
        gateway 2a03:b0c0:3:d0::1
        autoconf 0
        dns-nameservers 2001:4860:4860::8844 2001:4860:4860::8888 209.244.0.3
EOF