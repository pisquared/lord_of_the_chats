#!/usr/bin/env bash
echo "REMOTE: Cloning repo"
echo "======================================="
eval `ssh-agent`
ssh-add ~/.ssh/pi2_key_external
ssh-keyscan -t rsa gitlab.com >> ~/.ssh/known_hosts
if [ "$(ls -A /var/www/mafkingprod.com)" ]; then
    # directory not empty
    cd /var/www/mafkingprod.com
    git pull origin master
else
    git clone git@gitlab.com:pisquared/mafkingprod.git /var/www/mafkingprod.com
fi

cd /var/www/mafkingprod.com
cp ~/sensitive.py .

echo "REMOTE: Creating virtualenv"
echo "======================================="
virtualenv -p python3 venv
source venv/bin/activate

echo "REMOTE: Installing python packages"
echo "======================================="
pip install -r requirements.txt
pip install --ignore-installed setuptools==36.5.0  # fixes crash with babel

echo "REMOTE: Patching flask_markdown"
echo "======================================="
patch venv/lib/python3.5/site-packages/flaskext/markdown.py provision/markdown.patch
patch venv/lib/python3.5/site-packages/flask_cache/jinja2ext.py provision/flask_cache_jinja.patch
patch venv/lib/python3.5/site-packages/flask_sqlalchemy_cache/core.py provision/flask_sql_cache_core.patch

echo "REMOTE: updgrade db"
echo "======================================="
export FLASK_APP="webapp.py"
flask db upgrade
exit 0