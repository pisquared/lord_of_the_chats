#!/usr/bin/env bash
echo "REMOTE: Change ownership of private key"
echo "======================================="
chown -R mafkingprod_user:mafkingprod_user /home/mafkingprod_user/.ssh
chmod 700 /home/mafkingprod_user/.ssh
chmod 600 /home/mafkingprod_user/.ssh/*
