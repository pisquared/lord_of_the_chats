#!/usr/bin/env bash
echo "REMOTE: Installing nginx"
echo "======================================="
apt-get update
apt-get install -y nginx python-virtualenv

echo "REMOTE: Creating mafkingprod user"
echo "======================================="
useradd -m mafkingprod_user
mkdir -p /home/mafkingprod_user/.ssh
mkdir -p /var/www/mafkingprod.com
chown mafkingprod_user /var/www/mafkingprod.com