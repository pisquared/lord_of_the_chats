import os

basepath = os.path.dirname(os.path.realpath(__file__))
SQLALCHEMY_DATABASE_URI = 'sqlite:///{}'.format(os.path.join(basepath, "db.sqlite"))