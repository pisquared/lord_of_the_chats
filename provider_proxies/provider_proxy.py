class ProviderProxy(object):
    def authenticate(self, auth_id=None):
        pass

    def authenticate_verify(self, auth_id=None, verification_code=None):
        pass

    def is_authenticated(self):
        pass

    def get_auth_id(self):
        pass

    def get_auth_params(self):
        pass

    def send_message(self, from_=None, to=None, body=None):
        pass
