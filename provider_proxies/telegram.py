import json
import os

from flask_login import current_user
from telethon import TelegramClient, events, types
from telethon.tl.types import PeerUser

from provider_proxies.provider_proxy import ProviderProxy
from sensitive import TELEGRAM_API_HASH, TELEGRAM_API_ID
from webapp.models import UserProviderContact, Channel, Contact, Message
from webapp.store import db

api_id = TELEGRAM_API_ID
api_hash = TELEGRAM_API_HASH
telegram_root_dir = os.path.join('data', 'sessions', 'telegram')

os.makedirs(telegram_root_dir, exist_ok=True)


class TelegramProviderProxy(ProviderProxy):
    def __init__(self, user_provider):
        self.provider = user_provider.provider
        self.user_provider = user_provider
        telegram_session_path = os.path.join(telegram_root_dir, 'lotc_user_{}'.format(user_provider.id))
        self.client = TelegramClient(telegram_session_path, api_id, api_hash)
        self.client.connect()

    def authenticate(self, auth_id=None):
        auth_instance = self.client.sign_in(phone=auth_id)
        print(auth_instance)
        return {
            'phone_code_hash': auth_instance.phone_code_hash
        }

    def authenticate_verify(self, user_provider=None, verification_code=None):
        auth_id = user_provider.auth_id
        auth_params = json.loads(user_provider.auth_params)
        phone_code_hash = auth_params.get('phone_code_hash')
        auth_instance = self.client.sign_in(phone=auth_id, code=verification_code, phone_code_hash=phone_code_hash)
        print(auth_instance)
        return auth_params

    def is_authenticated(self):
        return self.client.is_user_authorized()

    def get_auth_id(self):
        me = self.client.get_me()
        if me:
            return '+{}'.format(me.phone)
        return ''

    def get_auth_params(self):
        me = self.client.get_me()
        if me:
            # TODO: what to return here?
            return {'phone_code_hash': ''}
        return {}

    def get_contacts(self):
        contact_me = Contact.get_by(user=current_user, me=True)
        dialogs = self.client.get_dialogs()
        for dialog in dialogs:
            entity_type = type(dialog.entity)
            provider_key_id = dialog.entity.id
            user_provider_contact = UserProviderContact.get_by(_raise_error=False,
                                                               provider_key_id=provider_key_id,
                                                               user=current_user,
                                                               user_provider=self.user_provider)
            last_name = ""
            if not user_provider_contact:
                if entity_type is types.Chat:
                    first_name = dialog.entity.title
                elif entity_type is types.User:
                    first_name = dialog.entity.first_name
                    last_name = dialog.entity.last_name
                elif entity_type is types.Channel:
                    first_name = dialog.entity.title
                else:
                    first_name = ""

                user_provider_contact = UserProviderContact.create(provider_key_id=provider_key_id,
                                                                   user_provider=self.user_provider,
                                                                   user=current_user,
                                                                   first_name=first_name,
                                                                   )

                if last_name:
                    user_provider_contact.last_name = last_name
                    db.session.add(user_provider_contact)
                channel_name = user_provider_contact.name
                channel = Channel.create(name=channel_name,
                                         user=current_user)
                if entity_type is types.User:
                    contact = Contact.create(first_name=first_name,
                                             default_channel=channel,
                                             user=current_user,
                                             )
                    if last_name:
                        contact.last_name = last_name
                        db.session.add(contact)
                    user_provider_contact.contact = contact
                    db.session.add(user_provider_contact)
                db.session.commit()

        for dialog in dialogs:
            entity_type = type(dialog.entity)
            provider_key_id = dialog.entity.id
            user_provider_contact = UserProviderContact.get_by(provider_key_id=provider_key_id,
                                                               user_provider=self.user_provider,
                                                               user=current_user,
                                                               )
            channel = Channel.get_by(
                _raise_error=False,
                user=current_user,
                name=user_provider_contact.name)
            if not channel:
                channel = Channel.create(user=current_user,
                                         name=user_provider_contact.name)
            telegram_messages = self.client.get_messages(entity=dialog.entity, limit=30)
            for telegram_message in telegram_messages:
                telegram_message = telegram_message
                body = telegram_message.raw_text
                date_sent = telegram_message.date
                provider_key_id = telegram_message.id
                provider_sender_id = telegram_message.sender.id
                is_me = telegram_message.sender.is_self
                if is_me:
                    from_contact = contact_me
                    from_provider_contact = UserProviderContact.get_by(user=current_user,
                                                                       user_provider=self.user_provider,
                                                                       provider_key_id=provider_sender_id)
                else:
                    from_provider_contact = UserProviderContact.get_by(_raise_error=False,
                                                                       user=current_user,
                                                                       user_provider=self.user_provider,
                                                                       provider_key_id=provider_sender_id)
                    if not from_provider_contact:
                        last_name = ""
                        if entity_type is types.Chat:
                            first_name = dialog.entity.title
                        elif entity_type is types.User:
                            first_name = dialog.entity.first_name
                            last_name = dialog.entity.last_name
                        elif entity_type is types.Channel:
                            first_name = dialog.entity.title
                        else:
                            first_name = ""
                        from_provider_contact = UserProviderContact.create(provider_key_id=provider_sender_id,
                                                                           user_provider=self.user_provider,
                                                                           user=current_user,
                                                                           first_name=first_name,
                                                                           )
                        if last_name:
                            user_provider_contact.last_name = last_name
                            db.session.add(user_provider_contact)
                    from_contact = from_provider_contact.contact
                Message.create(user=current_user,
                               to_channel=channel,
                               body=body,
                               provider_sent_ts=date_sent,
                               provider_key_id=provider_key_id,
                               from_contact=from_contact,
                               via_user_provider_contact=from_provider_contact,
                               )

    def _new_message(self, event, *args, **kwargs):
        print(event)

    def subscribe_to_new_messages(self):
        self.client.add_event_handler(self._new_message, events.NewMessage)

    def send_message(self, from_=None, to="", body=""):
        user_entity = self.client.get_entity(int(to))
        self.client.send_message(entity=user_entity, message=body)
