import base64
import os
import string
import random
import time
from hashlib import sha1

import requests
from requests.auth import HTTPBasicAuth

client_id = 4229


def gen_random_str(k):
    return ''.join(random.choice(string.ascii_uppercase + string.ascii_lowercase + string.digits) for _ in range(k))


def request_authentication(number):
    r = requests.get("https://textsecure-service.whispersystems.org/v1/accounts/sms/code/{}".format(number),
                     verify=False)
    print(r.status_code)
    print(r.text)


def verify_authentication(number, password, code):
    password = password or gen_random_str(16)
    print(password)

    aes_32_byte = os.urandom(32)
    print(aes_32_byte)

    hmac_sha1_mac_20_byte = os.urandom(20)
    print(hmac_sha1_mac_20_byte)

    concatenated_key = aes_32_byte + hmac_sha1_mac_20_byte
    print(concatenated_key)
    signaling_key = base64.b64encode(concatenated_key)
    print(signaling_key)

    r = requests.put("https://textsecure-service.whispersystems.org/v1/accounts/code/{}".format(code),
                     auth=HTTPBasicAuth(number, password),
                     json={
                         "signalingKey": "{}".format(signaling_key),
                         "supportsSms": False,
                         "registrationId": "{}".format(client_id)
                     },
                     verify=False)
    print(r.status_code)
    print(r.text)


def get_contacts(number, password):
    sh = sha1(bytes("+359988792054", "utf-8")).digest()
    print(sh)
    b64 = base64.b64encode(sh)
    print(b64)
    b64c = b64.split(b"=")[0][0:10]
    print(b64c)

    r = requests.put("https://textsecure-service.whispersystems.org/v1/directory/tokens",
                     auth=HTTPBasicAuth(number, password),
                     json={
                         "contacts": ["{}".format(b64c)]
                     },
                     verify=False)
    print(r.status_code)
    print(r.text)


def send_message(number, password, message):
    r = requests.put("https://textsecure-service.whispersystems.org/v1/messages/{}".format(number),
                     auth=HTTPBasicAuth(number, password),
                     json={"messages": [{
                         "type": "4",
                         "destinationDeviceId": "destinationDeviceId",
                         "destinationRegistrationId": "destinationRegistrationId",
                         "body": str(base64.b64encode(bytes(message, "utf-8"))),
                         "timestamp": int(time.time()),
                     }],
                     },
                     verify=False)
    print(r.status_code)
    print(r.text)


if __name__ == "__main__":
    final_number = "+359988792054"
    final_password = "LjfhsaSq2WSqyWqI"
    # request_authentication(final_number)
    verify_authentication(final_number, final_password, "173742")
    # get_contacts(final_number, final_password)
    # send_message(number, final_password, "Hello world!!")

