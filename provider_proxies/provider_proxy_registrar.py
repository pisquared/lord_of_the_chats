from flask import url_for
from flask_login import current_user

from provider_proxies.telegram import TelegramProviderProxy
from webapp.models import Provider, UserProvider


class ProviderProxyWrapper(object):
    def __init__(self, provider_proxy_class=None, authenticate_template=None, authenticate_verify_template=None):
        self.provider_proxy_class = provider_proxy_class
        self.authenticate_template = authenticate_template
        self.authenticate_verify_template = authenticate_verify_template


telegram_provider_proxy = ProviderProxyWrapper(provider_proxy_class=TelegramProviderProxy,
                                               authenticate_template="telegram/authenticate.html",
                                               authenticate_verify_template="telegram/authenticate_verify.html",
                                               )

PROVIDER_PROXIES_REGISTRY = {
    "telegram": telegram_provider_proxy,
}


class UserProviderException(Exception):
    def __init__(self, message, redirect_to):
        super().__init__(message)
        self.redirect_to = redirect_to


class UserProviderProxies(object):
    def __init__(self):
        self._registry = {}

    def _register(self, user_provider_id, provider_proxy):
        self._registry[user_provider_id] = provider_proxy

    def get(self, provider_id):
        try:
            provider = Provider.get_by(id=provider_id)
        except Exception as e:
            raise UserProviderException(str(e), url_for("client_bp.get_providers"))
        try:
            user_provider = UserProvider.get_by(user=current_user, provider=provider)
        except Exception as e:
            raise UserProviderException(str(e), url_for("client_bp.provider_authenticate", provider_id=provider_id))
        provider_proxy = self._registry.get(user_provider.id)
        if not provider_proxy:
            provider_proxy_wrapper = PROVIDER_PROXIES_REGISTRY.get(provider.proxy_name)
            provider_proxy = provider_proxy_wrapper.provider_proxy_class(user_provider)
            self._register(user_provider.id, provider_proxy)
        return provider_proxy


USER_PROVIDER_PROXIES = UserProviderProxies()
