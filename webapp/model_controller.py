import json
from datetime import datetime

from flask_login import current_user
from sqlalchemy.ext.declarative import declared_attr, DeclarativeMeta
from sqlalchemy_utils import InstrumentedList, Choice

from webapp.store import db
from webapp.utils import camel_case_to_snake_case


class ModelController(object):
    """
    This interface is the parent of all models in our database.
    """

    @declared_attr
    def __tablename__(self):
        return camel_case_to_snake_case(self.__name__)  # pylint: disable=E1101

    _sa_declared_attr_reg = {'__tablename__': True}
    __mapper_args__ = {'always_refresh': True}

    id = db.Column(db.Integer, primary_key=True)

    _excluded_serialization = []

    @classmethod
    def create(cls, log_class=None, **kwargs):
        now = datetime.utcnow()
        if issubclass(cls, Datable):
            kwargs['created_ts'] = now
        instance = cls(**kwargs)
        db.session.add(instance)
        if log_class:
            db.session.commit()
            log = log_class(body=u"[{}|{}|created] {}".format(instance.__class__.__name__, instance.id, repr(instance)),
                            created_ts=now,
                            )
            user = kwargs.get('user')
            if user:
                log.user = user
            db.session.add(log)
        db.session.commit()
        return instance

    @classmethod
    def get_by(cls, _raise_error=True, **kwargs):
        instance = cls.query.filter_by(**kwargs).first()
        if not instance and _raise_error:
            raise Exception("{} doesn't exist.".format(cls.__name__))
        return instance

    @classmethod
    def get_all_by(cls, force_archivable=False, **kwargs):
        return cls.query.filter_by(**kwargs).all()

    def serialize_flat(self, with_=None, depth=0, withs_used=None):
        """
        Serializes object to dict
        It will ignore fields that are not encodable (set them to 'None').

        It expands relations mentioned in with_ recursively up to MAX_DEPTH and tries to smartly ignore
        recursions by mentioning which with elements have already been used in previous depths
        :return:
        """

        MAX_DEPTH = 3

        def with_used_in_prev_depth(field, previous_depths):
            for previous_depth in previous_depths:
                if field in previous_depth:
                    return True
            return False

        def handle_withs(data, with_, depth, withs_used):
            if isinstance(data, InstrumentedList):
                if depth >= MAX_DEPTH:
                    return [e.serialize_flat() for e in data]
                else:
                    return [e.serialize_flat(with_=with_, depth=depth + 1, withs_used=withs_used) for e in data]
            else:
                if depth >= MAX_DEPTH:
                    return data.serialize_flat()
                else:
                    return data.serialize_flat(with_=with_, depth=depth + 1, withs_used=withs_used)

        if not with_:
            with_ = []
        if not withs_used:
            withs_used = []
        if isinstance(self.__class__, DeclarativeMeta):
            # an SQLAlchemy class
            fields = {}
            iterable_fields = [x for x in dir(self) if not x.startswith('_') and x not in ['metadata',
                                                                                           'item_separator',
                                                                                           'key_separator'] and x.islower()
                               and x not in self._excluded_serialization]
            for field in iterable_fields:
                data = self.__getattribute__(field)
                try:
                    if field in with_:
                        # this hanldes withs nested inside other models
                        if len(withs_used) < depth + 1:
                            withs_used.append([])
                        previous_depths = withs_used[:depth]
                        if with_used_in_prev_depth(field, previous_depths):
                            continue
                        withs_used[depth].append(field)
                        data = handle_withs(data, with_, depth, withs_used)
                    if isinstance(data, datetime):
                        data = str(data)
                    if isinstance(data, Choice):
                        data = data.code
                    json.dumps(data)  # this will fail on non-encodable values, like other classes
                    if isinstance(data, InstrumentedList):
                        continue  # pragma: no cover
                    fields[field] = data
                except TypeError:
                    pass  # Don't assign anything
            # a json-encodable dict
            return fields


class Ownable(object):
    @declared_attr
    def user_id(self):
        return db.Column(db.Integer, db.ForeignKey('user.id'))

    @declared_attr
    def user(self):
        return db.relationship("User")

    @classmethod
    def get_owned(cls):
        return cls.get_all_by(user=current_user)


class Datable(object):
    created_ts = db.Column(db.DateTime())
    modified_ts = db.Column(db.DateTime())
