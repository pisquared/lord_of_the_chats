from flask_cors import CORS
from flask_wtf.csrf import CSRFProtect

csrf = CSRFProtect()
cors = CORS()
