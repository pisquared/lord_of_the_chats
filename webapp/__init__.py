#!/usr/bin/env python
import os

from flask import Flask
from flask_security import Security, SQLAlchemyUserDatastore

import config

app = Flask(__name__)

app.config['SECRET_KEY'] = 'super-secret'
app.config['SECURITY_PASSWORD_SALT'] = 'super-secret'
app.config['SECURITY_REGISTERABLE'] = True
app.config['SECURITY_SEND_REGISTER_EMAIL'] = False
app.config['SECURITY_CHANGEABLE'] = True
app.config['SECURITY_SEND_PASSWORD_CHANGE_EMAIL'] = False

# override for prod
if os.environ.get("FLASK_WEBAPP_ENV") == "prod":
    import sensitive

    if sensitive.SECRET_KEY == "CHANGEME" \
            or sensitive.SECURITY_PASSWORD_SALT == "CHANGEME":
        raise Exception("Default security key detected, generate more secure ones.")
    app.config['DEBUG'] = False
    app.config['DEBUG_TB_ENABLED'] = False
    app.config['DEBUG_TB_PROFILER_ENABLED'] = False
    app.config['SECRET_KEY'] = sensitive.SECRET_KEY
    app.config['SECURITY_PASSWORD_SALT'] = sensitive.SECURITY_PASSWORD_SALT

app.config['SQLALCHEMY_DATABASE_URI'] = config.SQLALCHEMY_DATABASE_URI
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

app.config['CACHE_TYPE'] = 'simple'


def init_app():
    from webapp.csrf_protect import csrf, cors
    csrf.init_app(app)
    cors.init_app(app)

    from webapp.store import db
    db.init_app(app)

    from webapp.models import Role, User
    user_datastore = SQLAlchemyUserDatastore(db, User, Role)
    Security(app, user_datastore)

    from flask_cache import Cache
    from flask_sqlalchemy_cache import FromCache

    cache = Cache(app)

    @app.login_manager.user_loader
    def _load_user(id=None):
        return User.query.options(FromCache(cache)).filter_by(id=id).first()

    from webapp.jinja_filters import register_filters
    register_filters(app)

    from webapp.views import client_bp

    app.register_blueprint(client_bp)


init_app()
