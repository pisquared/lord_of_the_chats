import datetime
import json

from flask import redirect, url_for, render_template, flash, request
from flask_login import current_user, login_required

from provider_proxies.provider_proxy_registrar import PROVIDER_PROXIES_REGISTRY, USER_PROVIDER_PROXIES, \
    UserProviderException
from webapp.models import Provider, UserProvider, Channel, Message, Contact, UserProviderContact
from webapp.store import db
from webapp.views import client_bp


@client_bp.route('/')
def home():
    """
    If user is not authenticated - present with register/login page.
    If user is authenticated but no providers yet - redirect to providers page
    Else - redirect to channels page
    :return:
    """
    if current_user.is_authenticated:
        user_providers = UserProvider.get_all_by(user=current_user)
        if not user_providers:
            return redirect(url_for("client_bp.get_providers"))
        return redirect(url_for("client_bp.get_channels"))
    return redirect(url_for('security.register'))


@client_bp.route('/providers')
@login_required
def get_providers():
    """
    Shows a list of supported providers to chose to sign into
    """
    providers = Provider.get_all_by()
    user_providers = [up.provider_id for up in UserProvider.get_all_by(user=current_user)]
    return render_template("providers.html", providers=providers, user_providers=user_providers)


@client_bp.route('/providers/<int:provider_id>')
@login_required
def get_provider(provider_id):
    """
    Shows a sign up/login page for a provider
    or if already signed in - redirects to list of provider contacts
    """
    try:
        provider_proxy = USER_PROVIDER_PROXIES.get(provider_id)
    except UserProviderException as e:
        flash(str(e))
        return redirect(e.redirect_to)
    if not provider_proxy.is_authenticated():
        # user hasn't been authenticated with this provider, start the process
        return redirect(url_for("client_bp.provider_authenticate", provider_id=provider_id))
    # the user is authenticated, redirect
    provider = Provider.get_by(id=provider_id)
    return render_template("provider.html", provider=provider)


@client_bp.route('/providers/<int:provider_id>/authenticate', methods=["GET", "POST"])
@login_required
def provider_authenticate(provider_id):
    """
    Shows and submits the first authenticate step - e.g. phone number
    or if already signed in - redirects to the main provider page
    """
    # get this provider or redirect to page of providers
    try:
        provider = Provider.get_by(id=provider_id)
    except Exception as e:
        flash(str(e))
        return redirect(url_for("client_bp.get_providers"))
    # get this provider proxy
    provider_proxy_wrapper = PROVIDER_PROXIES_REGISTRY.get(provider.proxy_name)
    # this can safely fail as a UserProvider will not exist in the database on first query
    user_provider = UserProvider.get_by(_raise_error=False, user=current_user, provider=provider)
    # create if not exist
    if not user_provider:
        UserProvider.create(user=current_user, provider=provider)
    try:
        provider_proxy = USER_PROVIDER_PROXIES.get(provider_id)
    except UserProviderException as e:
        flash(str(e))
        return redirect(e.redirect_to)
    if provider_proxy.is_authenticated():
        # the user is authenticated, redirect
        return redirect(url_for('client_bp.get_provider', provider_id=provider_id))
    # user hasn't been authenticated with this provider, start the process
    if request.method == "POST":
        auth_id = request.form.get("auth_id")
        # try to authenticate with this provider
        try:
            auth_params = provider_proxy.authenticate(auth_id)
            auth_params = json.dumps(auth_params)
            user_provider = UserProvider.get_by(user=current_user, provider=provider)
            user_provider.auth_id = auth_id
            user_provider.auth_params = auth_params
            db.session.add(user_provider)
            db.session.commit()
        except Exception as e:
            flash(str(e))
            return render_template(provider_proxy_wrapper.authenticate_template, provider=provider)
        return redirect(url_for('client_bp.provider_verify', provider_id=provider_id))
    return render_template(provider_proxy_wrapper.authenticate_template, provider=provider)


@client_bp.route('/providers/<int:provider_id>/verify', methods=["GET", "POST"])
@login_required
def provider_verify(provider_id):
    """
    Submits the second authenticate step - e.g. verification number
    Shows a verify step for the authentication process of a provider (if exists)
    or if already signed in - redirects to list of provider contacts
    """
    try:
        provider = Provider.get_by(id=provider_id)
    except Exception as e:
        flash(str(e))
        return redirect(url_for("client_bp.get_providers"))
    # a user_provider should already exist, created in the prev step of authentication, fail otherwise
    try:
        user_provider = UserProvider.get_by(user=current_user, provider=provider)
    except Exception as e:
        flash("You need to authenticate first.")
        return redirect(url_for('client_bp.provider_authenticate', provider_id=provider_id))
    # get this provider proxy
    provider_proxy_wrapper = PROVIDER_PROXIES_REGISTRY.get(provider.proxy_name)
    try:
        provider_proxy = USER_PROVIDER_PROXIES.get(provider_id)
    except UserProviderException as e:
        flash(str(e))
        return redirect(e.redirect_to)
    if provider_proxy.is_authenticated():
        return redirect(url_for('client_bp.get_provider', provider_id=provider_id))
    if request.method == "POST":
        # continue with verifying the code for this auth_id
        verification_code = request.form.get("verification_code")
        try:
            auth_params = provider_proxy.authenticate_verify(user_provider, verification_code)
            auth_params = json.dumps(auth_params)
            # TODO: implement update method for db
            user_provider.auth_params = auth_params
            db.session.add(user_provider)
            db.session.commit()
        except Exception as e:
            flash(str(e))
            return render_template(provider_proxy_wrapper.authenticate_verify_template, provider=provider)
        return redirect(url_for("client_bp.get_provider", provider_id=provider_id))
    return render_template(provider_proxy_wrapper.authenticate_verify_template, provider=provider)


@client_bp.route('/providers/<int:provider_id>/contacts')
@login_required
def get_provider_contacts(provider_id):
    """
    Shows a list of this provider contacts
    """
    try:
        provider_proxy = USER_PROVIDER_PROXIES.get(provider_id)
    except Exception as e:
        flash(e)
        return redirect(url_for('client_bp.get_providers', provider_id=provider_id))
    if not provider_proxy.is_authenticated():
        return redirect(url_for('client_bp.provider_authenticate', provider_id=provider_id))
    provider_proxy.get_contacts()
    user_provider_contacts = UserProviderContact.get_all_by(user=current_user,
                                                            user_provider=provider_proxy.user_provider)
    return render_template("provider_contacts.html", user_provider_contacts=user_provider_contacts)


@client_bp.route('/contacts')
@login_required
def get_contacts():
    """
    Shows a list of contacts
    """
    contacts = Contact.get_all_by(user=current_user,
                                  )
    return render_template("contacts.html",
                           contacts=contacts)


@client_bp.route('/contacts/<int:contact_id>')
@login_required
def get_contact(contact_id):
    """
    Shows a specific contact details
    """
    return render_template("contact.html")


@client_bp.route('/providers/<int:provider_id>/contacts/<int:provider_contact_id>')
@login_required
def get_provider_contact(provider_id, provider_contact_id):
    """
    Shows details about this provider contact
    """
    return render_template("provider_contacts.html")


@client_bp.route('/providers/<int:provider_id>/contacts/<int:provider_contact_id>/merge_into_contact/<int:contact_id>')
@login_required
def merge_provider_contact_into_contact(provider_id, provider_contact_id, contact_id):
    """
    Merge this provider into this contact
    """
    return render_template("merge_contact.html")


@client_bp.route(
    '/providers/<int:provider_id>/contacts/<int:provider_contact_id>/unmerge_from_contact/<int:contact_id>')
@login_required
def unmerge_provider_contact_into_contact(provider_id, provider_contact_id, contact_id):
    """
    UnMerge this provider into this contact
    """
    return render_template("merge_contact.html")


@client_bp.route('/channels')
@login_required
def get_channels():
    """
    Shows a list of contacts to start a chat with along with their set statuses.
    :return: 
    """
    channels = Channel.get_all_by(user=current_user)
    return render_template("channels.html", channels=channels)


@client_bp.route('/channels/contact/<int:contact_id>')
@login_required
def channels_contact_default(contact_id):
    """
    Shows a channel with a contact.
    Sends a message on their default provider
    Receive messages from all providers
    """
    return render_template("channels_contact.html")


@client_bp.route('/channels/contact/<int:contact_id>/provider/<int:provider_id>')
@login_required
def channels_contact_provider(contact_id, provider_id):
    """
    Shows a channels with messages of a contact on the specified provider
    Send and receive messages only from that provider
    """
    return render_template("channels_contact_provider.html")


@client_bp.route('/channels/<int:channel_id>')
@login_required
def get_channel(channel_id):
    """
    Shows a channel.
    Sends a message to all participants of the channel to their configured provider for that channel
    Receive messages from all participants of the channel
    """
    try:
        channel = Channel.get_by(id=channel_id, user=current_user)
    except Exception as e:
        flash(str(e))
        return redirect(url_for("client_bp.get_channels"))
    channel_messages = Message.query. \
                           filter_by(user=current_user, to_channel=channel). \
                           order_by(Message.provider_sent_ts.desc()). \
                           all()[:20][::-1]
    channels = Channel.get_all_by(user=current_user)
    return render_template("channel.html",
                           channels=channels,
                           channel=channel,
                           channel_messages=channel_messages)


@client_bp.route('/message/contact/<int:contact_id>/provider/<int:provider_id>', methods=["POST"])
@login_required
def send_message_via_provider(contact_id, provider_id):
    """
    Sends a message to a contact on the specified provider
    """
    return ""


@client_bp.route('/message/channel/<int:channel_id>', methods=["POST"])
@login_required
def send_message_to_channel(channel_id):
    """
    Sends a message to a channel
    """
    try:
        channel = Channel.get_by(id=channel_id, user=current_user)
    except Exception as e:
        flash(str(e))
        return redirect(url_for("client_bp.get_channels"))
    body = request.form.get('body')
    from_contact = Contact.get_by(user=current_user, me=True)
    now = datetime.datetime.utcnow()
    Message.create(user=current_user,
                   from_contact=from_contact,
                   to_channel=channel,
                   body=body,
                   provider_sent_ts=now,
                   )
    channel_contact = channel.contact
    if channel_contact:
        channel_contact = channel_contact[0]
        for user_provider_contact in channel_contact.user_provider_contacts:
            provider_id = user_provider_contact.user_provider.provider.id
            provider_proxy = USER_PROVIDER_PROXIES.get(provider_id)
            provider_proxy.send_message(
                to=user_provider_contact.provider_key_id,
                body=body,
            )
    return redirect(url_for("client_bp.get_channel", channel_id=channel_id))


@client_bp.route('/set_mood', methods=["GET", "POST"])
@login_required
def set_mood_message():
    """
    Sets the current mood message
    """
    return ""
