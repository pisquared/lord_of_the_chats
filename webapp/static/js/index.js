$('#emoji-textarea').focus();
var REQUEST_SID = '';
var socket = io.connect('http://' + document.domain + ':' + location.port);

function scrollBottom() {
  $('.commited-messages').stop().animate({
    scrollTop: $('.commited-messages')[0].scrollHeight
  }, 800);
}

function initMessageContainer(sid) {
  $('#chat-messages').append("<div class='row' id='message-container-" + sid + "'></div>");
  $("#message-container-" + sid).append('' +
    '<div class="col-sm-12"><span class="badge badge-primary">' +
    '<span class="message-name"></span>' +
    '<span class="message-is-typing"></span>' +
    '</span> ' +
    '<span class="message-text"></span></div>');
}

function initCommitMessageContainer(ts) {
  $('#commited-messages').append("<div id='message-container-" + ts + "'></div>");
  $("#message-container-" + ts).append('<span class="badge badge-primary message-name"></span> <span class="message-text"></span>');
}

var emojione;
var emojioneContainerElement;

function writeMessage(sid, message) {
  if (emojione !== undefined) {
    emojione.ascii = true;
    var messageName = $('<div/>').text(message.name).html();
    var messageText = $('<div/>').text(message.message).html();
    $("#message-container-" + sid + " .message-name").html(emojione.toImage(messageName));
    $("#message-container-" + sid + " .message-is-typing").text(" is typing...");
    $("#message-container-" + sid + " .message-text").html(emojione.toImage(messageText));
    scrollBottom();
  } else {
    setTimeout(function () {
      writeMessage(sid, message)
    }, 100);
  }
}

function sendMessage() {
  socket.emit('message_s', {
    'name': $('#input-name').val(),
    'message': $('textarea').val()
  });
}

socket.on('sid', function (r) {
  REQUEST_SID = r.sid;
  console.log(REQUEST_SID);
  $('#chat-messages').append("<p id='message-" + REQUEST_SID + "'>");
});

socket.on('srv_connect', function (r) {
  console.log(">>> srv_connect");
  console.log(r);
  $.each(r.DB, function (k, v) {
    initMessageContainer(k);
    writeMessage(k, v);
  });
  $.each(r.COMMITED, function (_, k) {
    initCommitMessageContainer(k.ts);
    writeMessage(k.ts, k);
  })
});

// bind inputs
$("#input-name").on('keyup', sendMessage);

$('#commit-message').on('click', function () {
  var ts = Date.now();

  var message = {
    name: $('#input-name').val(),
    message: $('textarea').val(),
    ts: ts
  };
  socket.emit('message_commit', message);
  $("#delete-message").click();
});

// on new message
socket.on('message_r', function (m) {
  console.log(">>> message_r");
  console.log(m);
  var container = $('#message-container-' + m.sid);
  if (!container.length)
    initMessageContainer(m.sid);
  writeMessage(m.sid, m.m);
});

// on commited message
socket.on('message_commit', function (m) {
  console.log(">>> message_commit");
  console.log(m);
  initCommitMessageContainer(m.ts);
  writeMessage(m.ts, m);
});

$('#delete-message').on('click', function () {
  $('textarea').val("");
  emojioneContainerElement[0].emojioneArea.setText("");
  sendMessage();
});

// anulate is typing message
$(".message-is-typing").text("");
window.setInterval(function () {
  $(".message-is-typing").text("");
}, 1000);

$(document).ready(function () {
  emojioneContainerElement = $("textarea").emojioneArea({
    container: "#emoji-textarea",
    events: {
      keyup: function (editor, event) {
        var content = this.getText();
        $("textarea").val(content);
        if (event.keyCode === 13 && /\S/.test(content)) {
          $("#commit-message").click();
        }
        sendMessage();
      }
    }
  });
});