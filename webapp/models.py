from flask_security import RoleMixin, UserMixin

from webapp.model_controller import ModelController, Ownable, Datable
from webapp.store import db

roles_users = db.Table('roles_users',
                       db.Column('user_id', db.Integer(), db.ForeignKey('user.id')),
                       db.Column('role_id', db.Integer(), db.ForeignKey('role.id')))


class Role(db.Model, RoleMixin):
    id = db.Column(db.Integer(), primary_key=True)
    name = db.Column(db.String(80), unique=True)
    description = db.Column(db.String(255))


class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(255), unique=True)
    password = db.Column(db.String(255))

    timezone = db.Column(db.String, default='UTC')
    tz_offset_seconds = db.Column(db.Integer, default=0)

    active = db.Column(db.Boolean())
    confirmed_at = db.Column(db.DateTime())

    roles = db.relationship('Role', secondary=roles_users,
                            backref=db.backref('users', lazy='dynamic'))


class Provider(db.Model, ModelController):
    proxy_name = db.Column(db.Unicode)
    name = db.Column(db.Unicode)


class UserProvider(db.Model, ModelController, Ownable, Datable):
    auth_id = db.Column(db.Unicode)
    auth_params = db.Column(db.Unicode)

    provider_id = db.Column(db.Integer, db.ForeignKey('provider.id'))
    provider = db.relationship("Provider", backref="user_providers")


class Contact(db.Model, ModelController, Ownable, Datable):
    """
    Represents a merged contact from several providers
    """
    me = db.Column(db.Boolean, default=False)

    first_name = db.Column(db.Unicode)
    last_name = db.Column(db.Unicode)
    profile_image = db.Column(db.Unicode)

    mood_message = db.Column(db.Unicode)
    mood_message_ts = db.Column(db.DateTime)

    default_channel_id = db.Column(db.Integer, db.ForeignKey('channel.id'))
    default_channel = db.relationship("Channel", backref="contact", uselist=False)

    @property
    def name(self):
        if self.first_name and self.last_name:
            return "{} {}".format(self.first_name, self.last_name)
        elif self.first_name:
            return self.first_name
        else:
            return self.last_name


class UserProviderContact(db.Model, ModelController, Ownable, Datable):
    """
    Represents a contact from a provider
    """
    # key that is used to match with provider
    provider_key_id = db.Column(db.Unicode)

    first_name = db.Column(db.Unicode)
    last_name = db.Column(db.Unicode)
    profile_image = db.Column(db.Unicode)

    user_provider_id = db.Column(db.Integer, db.ForeignKey('user_provider.id'))
    user_provider = db.relationship("UserProvider", backref="provider_contacts")

    contact_id = db.Column(db.Integer, db.ForeignKey('contact.id'))
    contact = db.relationship("Contact", backref="user_provider_contacts")

    @property
    def name(self):
        if self.first_name and self.last_name:
            return "{} {}".format(self.first_name, self.last_name)
        elif self.first_name:
            return self.first_name
        else:
            return self.last_name


class Channel(db.Model, ModelController, Ownable, Datable):
    """
    Represents a place to chat 
    """
    name = db.Column(db.Unicode)


class Message(db.Model, ModelController, Ownable, Datable):
    provider_key_id = db.Column(db.Unicode)

    from_contact_id = db.Column(db.Integer, db.ForeignKey('contact.id'))
    from_contact = db.relationship("Contact", backref="messages")

    via_user_provider_contact_id = db.Column(db.Integer, db.ForeignKey('user_provider_contact.id'))
    via_user_provider_contact = db.relationship("UserProviderContact", backref="messages")

    to_channel_id = db.Column(db.Integer, db.ForeignKey('channel.id'))
    to_channel = db.relationship("Channel", backref="messages")

    body = db.Column(db.Unicode)
    provider_sent_ts = db.Column(db.DateTime)

